<?php

namespace App\Http\Controllers\candidate;

Use App\User;
Use App\Skill;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CandidateController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\CandidateMiddleware']);
    }
    public function show_candiddate(){
        $candidate = User::where('id',Auth::user()->id)->first();
 
         return view('candidate.show-profile')->with(['candidate'=>$candidate]);
     }
     
}
