<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
Use App\User;
Use App\Skill;
use Carbon\Carbon;
use Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCandidateRequest;
use App\Http\Requests\Admin\UpdateCandidateRequest;
class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\AdminMiddleware']);
    }

    public function manage_candidate(Request $request){
        return view('admin.manage-candidate');
    }

    public function show_add_candiddate(){
        $skills = Skill::all();
        return view('admin.add-candidate')->with(['skills'=>$skills]);
    }
    public function show_candiddate($id){
       $candidate = User::where('id',$id)->first();

        return view('admin.show-candidate')->with(['candidate'=>$candidate]);
    }


    public function store_candiddate(StoreCandidateRequest $request){
        $password = Hash::make($request->password);

        if($files=$request->file('cv_file')){
            $cv_fileName=$files->getClientOriginalName();
            $cv_fileName = time().'.'.$request->cv_file->getClientOriginalExtension();
            $files->move(public_path('cv_files'), $cv_fileName);
            $cv_fileExt = $files->getClientOriginalExtension();
            if($cv_fileExt=='docx'){
                $content = docx_to_text(public_path().'/cv_files/'.$cv_fileName);
            }
            if($cv_fileExt=='pdf'){
                $content = pdf_to_text(public_path().'/cv_files/'.$cv_fileName);
            }
            }else{
            $cv_fileName='';
            $cv_fileExt='';
            $content='';
            }

        // $cv_fileName = time().'.'.$request->cv_file->getClientOriginalExtension();
        // $request->cv_file->move(public_path('cv_files'), $cv_fileName);
        // $cv_fileExt = $request->cv_file->getClientOriginalExtension();

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = $password; //hashed password.
        if($user->skills && count($user->skills)!=0){
            $user->skills = implode(', ', $request->skills);
        }
        $user->cv_file = $cv_fileName;
        $user->cv_file_ext = $cv_fileExt;
        $user->cv_text = $content;
        $user->email_verified_at = Carbon::now();
        $user->save();
        return redirect()->route('admin.manage.candidate')->with('status', 'Candidate added Successfuly');
    }
    public function show_edit_candiddate($id){
        // print_r($id);
        // die;
        $skills = Skill::all();
        $candidate =User::where('role',1)->where('id',$id)->first();
        return view('admin.edit-candidate')->with([
            'skills'=>$skills,
            'candidate'=>$candidate,
            ]);
    }

    public function update_candiddate(UpdateCandidateRequest $request){

        if($files=$request->file('cv_file')){
            $cv_fileName=$files->getClientOriginalName();
            $cv_fileName = time().'.'.$request->cv_file->getClientOriginalExtension();
            $files->move(public_path('cv_files'), $cv_fileName);
            $cv_fileExt = $files->getClientOriginalExtension();
            if($cv_fileExt=='docx'){
                $content = docx_to_text(public_path().'/cv_files/'.$cv_fileName);
            }
            if($cv_fileExt=='pdf'){
                $content = pdf_to_text(public_path().'/cv_files/'.$cv_fileName);
            }
        }else{
            $cv_fileName='';
            $cv_fileExt='';
            $content='';
        }
        $user = User::where('id',$request->id)->first();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        if($user->skills && count($user->skills)!=0){
            $user->skills = implode(', ', $request->skills);
        }
        if(isset($request->password) && $request->password != ''){
            $user->password = Hash::make($request->password); //hashed password.
        }
        if($files=$request->file('cv_file')){
        $user->cv_file = $cv_fileName;
        $user->cv_file_ext = $cv_fileExt;
        $user->cv_text = $content;
        }
        $user->email_verified_at = Carbon::now();
        $user->save();


        return redirect()->route('admin.manage.candidate')->with('status', 'Candidate Updated Successfuly');
    }
    public function delete_candiddate($id){
        User::where('id',$id)->delete();
        return redirect()->route('admin.manage.candidate')->with('status', 'Candidate Deletes Successfuly');
    }








}
