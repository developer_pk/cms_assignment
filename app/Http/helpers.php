<?php 
use Smalot\PdfParser\Parser;

if (! function_exists('docx_to_text')) {
    function docx_to_text($path){
        $striped_content = '';
        $content = '';

        $zip = zip_open($path);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        return $outtext = strip_tags($content);

    }
}
if (! function_exists('pdf_to_text')) {
    function pdf_to_text($path){
        $PDFParser = new Parser();

        // Create an instance of the PDF with the parseFile method of the parser
        // this method expects as first argument the path to the PDF file
        $pdf = $PDFParser->parseFile($path);
        
        // Extract ALL text with the getText method
        return $text = $pdf->getText();

    }
}