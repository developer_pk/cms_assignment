@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Manage Candidate <a href="{{ route('admin.add.candidate') }}" class="btn btn-success float-sm-right">Add Candidate</a> </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <input type="text" name="skills" class="form-control searchSkills" placeholder="Search for skills Only...">
                    <br>
                    <br>
                   
                    <table id="users" class="table table-hover table-condensed" style="width:100%">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Skills</th>
                                <th>Resume</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-script')
<script type="text/javascript">
    jQuery(function () {
        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
      
      var table = jQuery('#users').DataTable({
          processing: true,
          serverSide: true,
         // ajax: "{{ route('admin.get.candidate') }}",
          ajax: {
            url: "{{ route('admin.get.candidate') }}",
            data: function (d) {
                  d.s_skills = $('.searchSkills').val(),
                  d.search = $('input[type="search"]').val()
              }
          },
          columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'first_name', name: 'first_name'},
              {data: 'last_name', name: 'last_name'},
              {data: 'email', name: 'email'},
              {data: 'skills', name: 'skills'},
              {data: 'cv_status', name: 'cv_status'},
              {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
     
      $(".searchSkills").keyup(function(){
            table.draw();
        });
    });
  </script>
  @stop




{{--  {data: 'action', name: 'action', orderable: false, searchable: false},  --}}


