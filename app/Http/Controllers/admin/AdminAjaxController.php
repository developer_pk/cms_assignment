<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Datatables;
use DB;
Use App\User;
Use App\Skill;
use Str;
use App\Http\Controllers\Controller;

class AdminAjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','App\Http\Middleware\AdminMiddleware']);
    }
    public function get_candidates(Request $request){
        $candidates =User::where('role',1)->get();
        if ($request->ajax()) {
            $data = User::where('role',1)->get();
            return datatables()->of($data)
                    ->addIndexColumn()
                    ->filter(function ($instance) use ($request) {
                        if (!empty($request->get('s_skills'))) {
                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                                if (Str::contains(Str::lower($row['skills']), Str::lower($request->get('s_skills')))){
                                    return true;
                                }else if (Str::contains(Str::lower($row['cv_text']), Str::lower($request->get('s_skills')))) {
                                    return true;
                                }
   
                                return false;
                            });
                        }
   
                        if (!empty($request->get('search'))) {
                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                                if (Str::contains(Str::lower($row['skills']), Str::lower($request->get('search')))){
                                    return true;
                                }else if (Str::contains(Str::lower($row['first_name']), Str::lower($request->get('search')))) {
                                    return true;
                                }else if (Str::contains(Str::lower($row['last_name']), Str::lower($request->get('search')))) {
                                    return true;
                                }else if (Str::contains(Str::lower($row['email']), Str::lower($request->get('search')))) {
                                    return true;
                                }
   
                                return false;
                            });
                        }
   
                    })                    
                    ->addColumn('cv_status', function($row){ 
                        if($row->cv_file){
                            $f_url = public_path().'/cv_files/'.$row->cv_file;
                            return '<a href="'.$f_url.'">Download Resume</a>';
                        }else{
                            return 'Resume not Uploaded';
                        }
                        
                    })
                    ->addColumn('action', function($row){   
                           $btn = '<a href="'.route("admin.show.candidate",$row->id).'" class="edit btn btn-info btn-sm ml-1">View</a>';
                           $btn .= '<a href="'.route("admin.edit.candidate",$row->id).'" class="edit btn btn-primary btn-sm ml-1">Edit</a>';
                           $btn .= '<a href="'.route("admin.delete.candidate",$row->id).'" class="edit btn btn-danger btn-sm ml-1">Delete</a>';
     
                            return $btn;
                    })
                    ->rawColumns(['cv_status','action'])
                    ->make(true);
        }
    }
}
