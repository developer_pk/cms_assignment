@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Candidate : <b>{{ $candidate->first_name.' '.$candidate->last_name }}</b> <a href="{{ route('admin.manage.candidate') }}" class="btn btn-success float-sm-right">List All Candidates</a> </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('admin.update.candidate') }}" enctype="multipart/form-data">
                        @csrf
                         <input type="hidden" name="id" value="{{ $candidate->id }}">

                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ $candidate->first_name }}" required autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $candidate->last_name }}" required autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $candidate->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cv_file" class="col-md-4 col-form-label text-md-right">{{ __('Skills') }}</label>

                            <div class="col-md-6">
                               
                                @if($candidate->skills)
                                @php
                                $c_skills = explode(', ', $candidate->skills);
                                @endphp
                                <select class="skills form-control" name="skills[]"  multiple>
                                        <option disabled>Select Skills</option>
                                        
                                    @foreach($skills as $skill)
                                        <option value="{{ $skill->name }}" <?= (in_array($skill->name, $c_skills) ? 'selected' : '') ?>>{{ $skill->name }}</option>
                                    @endforeach
                                  </select>
                                  @else
                                  <select class="skills form-control" name="skills[]"  multiple>
                                        <option selected disabled>Select Skills</option>
                                        
                                    @foreach($skills as $skill)
                                        <option value="{{ $skill->name }}">{{ $skill->name }}</option>
                                    @endforeach
                                  </select>
                                  @endif
                            </div>
                            @error('skills')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>

                       

                        <div class="form-group row">
                            <label for="cv_file" class="col-md-4 col-form-label text-md-right">{{ __('Upload CV') }}</label>

                            <div class="col-md-6">
                                <input id="cv_file" type="file" class="form-control" name="cv_file" accept=
                                "application/msword, application/pdf">
                            </div>
                            @error('cv_file')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            @if($candidate->cv_file)
                            <div class="col-md-6 offset-md-4">
                            <br/>
                            <b>Resume: <a href="{{asset('cv_files').'/'.$candidate->cv_file }}">Download Resume</a></b>
                            </div>
                            @else
                            <div class="col-md-6 offset-md-4">
                                    <br/>
                                    <b>No resume uploaded by candidate</b>
                                    </div>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update Candidate') }}
                                </button>
                            </div>
                        </div>
                    </form>
                   
                   

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-script')

@stop






