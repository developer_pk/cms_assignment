@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (auth()->user()->role==2)

                     <span class="title"> Wellcome {{auth()->user()->first_name}} {{auth()->user()->last_name}} </span>

                      <div class="links">
                        <a href="{{ route('admin.manage.candidate') }}" class="btn btn-info">Manage Candidate</a>
                        
                     </div>

                     @else(auth()->user()->role==1) 

                     <span class="title">   Wellcome {{auth()->user()->first_name}} {{auth()->user()->last_name}}</span>
                     <div class="links">
                            <a href="{{ route('candidate.profile') }}" class="btn btn-info">My Profile</a>
                            
                         </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
