## Candidate Management System

This simple management system for the candidate and main role in this system is admin because admin manages the candidate perform CRUD operation.

In the admin side, admin can search the skills in the table and in the content of the resume file of uploaded by candidate in the format of docx and pdf only.

I use a simple system for advanced searching when a candidate or admin uploads the resume I extract file content and store it in the database table.

This increases the searching process because when the number candidate increase every loop iteration document will parse the searching speed will be slow down so I use this process.


## How install and run code

Step 1: cd cms_assignment

Step 2: composer install

Step 3: npm install

Step 4: npm run dev

Step 5: use command cp .env.example .env  and config the database access

Step 6: php artisan key:generate

Step 7: php artisan migriate --seed

Step 8: php artisan serve 

## Credentials

Admin:
Email: admin@admin.com
Password: 123456789

Candidate:
Email: candidate@candidate.com
Password: 123456789

Thanks 

Regards 
Pankaj Kumar




