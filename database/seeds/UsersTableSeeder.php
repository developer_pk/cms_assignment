<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
              'first_name' => 'Super',
              'last_name' => 'Admin',
              'email' => 'admin@admin.com',
              'role' => 2,
              'password' => bcrypt('123456789'),
              'skills' => null,
              'cv_file' => null,
              'cv_file_ext' => null,
              'cv_text' => null,
            ),
            array(
                'first_name' => 'Pankaj',
                'last_name' => 'Kumar',
                'email' => 'candidate@candidate.com',
                'role' => 1,
                'password' => bcrypt('123456789'),
                'skills' => 'Php, Go, Ruby',
                'cv_file' => '1573234714.docx',
                'cv_file_ext' => 'docx',
                'cv_text' => '
                RESUME
                 
                Name                             :     Pankaj Kumar
                Father Name                 :      Sh. Daulat Ram
                Date of Birth                 :      06th September 1990
                Address                         :      Vill-Tikkri, PO-Panjgain, Teh-Sadar,
                                                             Distt-Bilaspur, Himachal Pradesh
                                                             Pin-174012
                Contact no.                  :       +91-9816971001, +91-9418738809
                Email ID                      :       developer.pk@hotmail.com,
                                                              pk.code.xpert@gmail.com
                Marital Status             :        Married
                Nationality                  :        Indian
                
                CARRIER OBJECTIVE
                Goal-oriented Web Developer with strong commitment to collaboration and solutions-oriented problem-solving. Solution-driven professional excelling in highly collaborative work environment, finding solutions to challenges and focused on customer satisfaction. Proven experience developing consumer-focused web sites using HTML, CSS, JQuery, PHP Framework (Laravel, Codeigniter) and JavaScript. Experience building products for Web Apps, meeting highest standards for web design, user experience, best practices, usability and speed. Responding to challenges by designing and developing solutions and building web applications aligned to customer&apos;s services. Translating solutions into code and working across many different APIs, third-party integrations and databases.
                CAPABILITIES
                *Hard working.
                *Honesty is the best Policy.
                *Ready to work for long hours.
                *Can work alone and in team environment.
                
                
                QUALIFICATIONS
                10th H.P board Dharamshala in 2008 with 72.67%.
                Dip. In Information Technology in 2008-11 with  54.51%.
                B.TECH. In Computer Science Engineering in 2013-15 with 63%
                
                WEB-TECHNOLOGIES AND SKILLS 
                Framework (Laravel, Codeigniter), HTML/CSS, Wordpress(CMS), jQuery and MySQL.
                COMPANY EXPERIENCE
                PHP Developer In Snowflakes Software Private Limited ( Plot No. 74 Sector 82, JLPL, Industrial Area, Sahibzada Ajit Singh Nagar, Punjab 160055 ). (08 June 2019 to 17 Oct 2019)
                Internship from 1-Jan-2017 at Talentelgia Pvt Ltd Mohali 67 as fresher in LAMP STACK and PHP framework (Laravel).
                TRANING EXPERIENCE
                During B.Tech. Fourth Semester 45 day Industrial Training (01 July 2013 to 14 Aug 2013) in (WEB DEVELOPMENT) PHP (Wordpress) from ZCC Group of Corporate Training &amp; Technical Education Chandigarh.
                During B.Tech. Sixth Semester One Month Industrial Training (01 July 2014 to 31 July 2014) in (WEB DEVELOPMENT) PHP from CCETT (CENTRAL COMMITTEE OF EDUCATION AND TECHNICAL TRAINING) Chandigarh.
                EXTRA ACTIVITIES
                2nd Position in C-WONK (code fusion) participated in SIRDA TECHFEST held at SIRDA campus.
                1st Position Softogenesis we develop the CALCULATOR in JAVA.
                Hobby:      
                Technological researches and R&amp;D in Latest Technologies, Hardworking, Solve Rubik&apos;s cube.
                 I hereby declare that the information stated as above is correct and true as per knowledge and belief.
                
                
                Dated: __/__/__                                                                             Signature: Pankaj Kumar
                ',                
                
            ),
          ));
    }
}
