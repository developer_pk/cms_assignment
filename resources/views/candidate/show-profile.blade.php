@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">My Profile</b></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                     
                        <div class="card-body">
                            <h4 class="card-title">{{ $candidate->first_name }} {{ $candidate->last_name }}</h4>
                            <p class="card-text"><b>Email:</b> {{ $candidate->email }}<br/>
                            <b>Resume: <a href="{{asset('cv_files').'/'.$candidate->cv_file }}">Download Resume</a></b>
                            </p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('page-script')

@stop






