<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->integer('role')->default(1)->comment('1:Candidate,2:Super Admin');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->longText('skills')->nullable();
            $table->longText('cv_file')->nullable();
            $table->string('cv_file_ext')->nullable();
            $table->longText('cv_text')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
