<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRegisterRequest;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(UserRegisterRequest $request)
        {
            //   print_r($request->file('cv_file'));
            //   die;
        $password = Hash::make($request->password);

        if($files=$request->file('cv_file')){
            $cv_fileName=$files->getClientOriginalName();
            $cv_fileName = time().'.'.$request->cv_file->getClientOriginalExtension();
            $files->move(public_path('cv_files'), $cv_fileName);
            $cv_fileExt = $files->getClientOriginalExtension();
            if($cv_fileExt=='docx'){
                $content = docx_to_text(public_path().'/cv_files/'.$cv_fileName);
            }
            if($cv_fileExt=='pdf'){
                $content = pdf_to_text(public_path().'/cv_files/'.$cv_fileName);
            }

            }else{
            $cv_fileName='';
            $cv_fileExt='';
            $content='';
            }

        // $cv_fileName = time().'.'.$request->cv_file->getClientOriginalExtension();
        // $request->cv_file->move(public_path('cv_files'), $cv_fileName);
        // $cv_fileExt = $request->cv_file->getClientOriginalExtension();

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = $password; //hashed password.
        if($user->skills && count($user->skills)!=0){
            $user->skills = implode(', ', $request->skills);
        }

        $user->cv_file = $cv_fileName;
        $user->cv_file_ext = $cv_fileExt;
        $user->cv_text = $content;
        $user->email_verified_at = Carbon::now();
        $user->save();

        //login as well.
        Auth::login($user,true);

        if (auth()->user()->role==2) {
             return redirect()->route('home')->with('status', 'Login Successfully');
         } else if (auth()->user()->role==1) {
             return redirect()->route('home')->with('status', 'Login Successfully');
         } else {
             Auth::logout();
             return redirect()->route('login')->with('status', 'Contact admin your role not matched');
         }
        Auth::logout();
    return redirect()->route('login')->with('status', 'Contact admin your role not matched');
        }


    public function showRegistrationForm(Request $request){
         $skills = Skill::all();
        return view('auth.register')->with(['skills'=>$skills]);
    }

}
