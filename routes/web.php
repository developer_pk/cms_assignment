<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// welcome route
Route::get('/', function () {
    return view('welcome');
});

// Auth route
Auth::routes();

// Main routes
Route::get('/home', 'HomeController@index')->name('home');

// Admin Routes
Route::group(['prefix' => 'admin','middleware' => 'App\Http\Middleware\AdminMiddleware','as'=>'admin.'], function()
{
    // View and Controller routes
    Route::get('manage-candidate', 'admin\AdminController@manage_candidate')->name('manage.candidate');
    Route::get('add-candidate', 'admin\AdminController@show_add_candiddate')->name('add.candidate');
    Route::get('candidate/{id}/edit', 'admin\AdminController@show_edit_candiddate')->name('edit.candidate');
    Route::post('store-candidate', 'admin\AdminController@store_candiddate')->name('store.candidate');
    Route::post('update-candidate', 'admin\AdminController@update_candiddate')->name('update.candidate');
    Route::get('candidate/{id}/delete', 'admin\AdminController@delete_candiddate')->name('delete.candidate');
    Route::get('candidate/{id}/view', 'admin\AdminController@show_candiddate')->name('show.candidate');
    
    //Data Table Ajax routes
    Route::get('get-candidate', 'admin\AdminAjaxController@get_candidates')->name('get.candidate');




});
// Candidates Routes
Route::group(['prefix' => 'candidate','middleware' => 'App\Http\Middleware\CandidateMiddleware','as'=>'candidate.'], function()
{
    Route::get('profile', 'candidate\CandidateController@show_candiddate')->name('profile');
});
