<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userskills extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    **/
    protected $table = 'user_skills';
}
