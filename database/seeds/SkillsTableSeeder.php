<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert(array(
            array(
              'name' => 'Php',
            ),
            array(
              'name' => 'Database',
            ),
            array(
                'name' => 'Go',
              ),
            array(
                'name' => 'Ruby',
              ), 
            array(
                'name' => 'Flutter',
              ), 
            array(
                'name' => 'Node js',
              ),
            array(
                'name' => 'Vue js',
              ),
          ));
    }
}
